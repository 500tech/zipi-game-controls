import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';

export default class Control extends PureComponent {
	static propTypes = {
		text: PropTypes.oneOfType([PropTypes.string.isRequired, PropTypes.node.isRequired]),
		inlineStyle: PropTypes.object,
		width: PropTypes.number,
		height: PropTypes.number,
		onClick: PropTypes.func.isRequired,
		isSelected: PropTypes.bool
	};

	static defaultProps = {
		inlineStyle: {},
		width: 23,
		height: 16
	};

	render() {
		return (
			<div
				onClick={this.props.onClick}
				autoFocus={this.props.autofocus}
				style={ 
					{
						backgroundColor: this.props.isSelected ? '#ec9b1d' : 'white',
						color: this.props.isSelected ? 'white' : 'black',
						height: `${this.props.height}px`,
						lineHeight: `${this.props.height}px`,
						width: `${this.props.width}px`,
						border: '1px solid #AAA',
						borderRadius: 3,
						margin: '1px',
						display: 'inline-block',
						cursor: 'pointer',
						fontSize: 10,
						textAlign: 'center',
						...this.props.inlineStyle
					}
				}
			>
				{this.props.text}
			</div>
		);
	}
}
