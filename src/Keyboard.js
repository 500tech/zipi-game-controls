import React, {PureComponent} from 'react';
import Control from './Control';
import PropTypes from 'prop-types';

const row1 = 	[
	'Esc',
	'F1',
	'F2',
	'F3',
	'F4',
	'F5',
	'F6',
	'F7',
	'F8',
	'F9',
	'F10',
	'F11',
	'F12'
].map((k) => ({ code: k, height: 25 })).concat([{
	code: 'power',
	value: 'Power',
	inlineStyle: {
		fontSize: 9,
		flexGrow: 1
	},
}]);

const row2 = [
	'~',
	'1',
	'2',
	'3',
	'4',
	'5',
	'6',
	'7',
	'8',
	'9',
	'0',
	'-',
	'+'
].map((k) => ({ code: k })).concat([{
	code: 'Del',
	inlineStyle: {
		fontSize: 9,
		flexGrow: 1
	}
}]);
const row3 = [
	{
		code: 'Tab',
		inlineStyle: {
			flexGrow: 1,
			fontSize: 9
		}
	}
].concat([
	'Q',
	'W',
	'E',
	'R',
	'T',
	'Y',
	'U',
	'I',
	'O',
	'P',
	'[',
	']',
	'\\'
].map((k) => ({ code: k })));

const row4 = [
	{
		code: 'CAPS',
		inlineStyle: {
			flexGrow: 1,
			fontSize: 9
		}
	}
].concat([
	'A',
	'S',
	'D',
	'F',
	'G',
	'H',
	'J',
	'K',
	'L',
	';',
	'\''
].map((k) => ({ code: k }))).concat([{
	code: 'Enter',
	inlineStyle: {
		fontSize: 9,
		flexGrow: 1		
	}
}]);

const row5 = [
	{
		code: 'leftShift',		
		value: 'Shift',
		inlineStyle: {
			fontSize: 9,
			flexGrow: 1		
		}
	}
].concat([
	'Z',
	'X',
	'C',
	'V',
	'B',
	'N',
	'M',
	',',
	'.',
	'/'
].map((k) => ({ code: k }))).concat([
	{
		code: 'up',
		value: '/\\',
		inlineStyle: {
		}
	},
	{
		code: 'rightShift',
		value: 'Shift',
		inlineStyle: {
			fontSize: 9
		}
	}
]);

const row6 = [
	{
		code: 'leftCtrl',
		value: 'Ctrl',
		inlineStyle: {
			fontSize: 9
		}
	},
	{
		code: 'Fn',
		inlineStyle: {
			fontSize: 9
		}
	},
	{
		code: 'leftAlt',
		value: 'Alt',
		inlineStyle: {
			fontSize: 9
		}
	},
	{
		code: 'Space',
		inlineStyle: {
			flexGrow: 1
		}
	},
	{
		code: 'rightShift',
		value: 'Alt',
		inlineStyle: {
			fontSize: 9
		}
	},
	{
		code: 'rightCtrl',
		value: 'Ctrl',
		inlineStyle: {
			fontSize: 9
		}
	},
	{
		code: 'left',
		value: '<',
		inlineStyle: {
		}
	},
	{
		code: 'down',
		value: '\\/',
		inlineStyle: {
		}
	},
	{
		code: 'right',
		value: '>',
		inlineStyle: {
		}
	}
];

const layout = [
	row1,
	row2,
	row3,
	row4,
	row5,
	row6
];

const mouseLeft = {
	inlineStyle: {
		position: 'absolute',
		left: '6px',
    top: '7px',
    width: '35px',
		borderRadius: 0,
		borderTopLeftRadius: 26,
		borderColor: 'black',
    height: '50px'
	},
	code: 'mouseLeft',
	value: ' '
}

const mouseCenter = {
	inlineStyle: {
		position: 'absolute',
    left: '34px',
    top: '14px',
		width: '15px',
    borderRadius: '20px',
		height: '30px'
	},
	code: 'mouseCenter',
	value: ' '
}

const mouseRight = {
	inlineStyle: {
		position: 'absolute',
		left: '42px',
    top: '7px',
    width: '34px',
		borderRadius: 0,
		borderTopRightRadius: 26,
		borderColor: 'black',
    height: '50px'
	},
	code: 'mouseRight',
	value: ' '
}

export default class Keyboard extends PureComponent {
	isSelected = (code) => {
		return this.props.selectedControls[code];
	}

	handleClick = (code) => () => {
		if (this.props.onSelect) {
			this.props.onSelect(code, !this.isSelected(code));
		}
	}

	renderControl = control => (
		<Control
			key={control.code}
			isSelected={ this.isSelected(control.code) }
			text={ control.value || control.code }
			onClick={ this.handleClick(control.code) }
			inlineStyle={ control.inlineStyle }
			width={ control.width }/>
	);

	render() {
		return (
			<div className="controls" style={ { display: 'flex' } }>
				<div className="keyboard">
					{
						layout.map((keys, index) => (
							<div key={index} style={ {display: 'flex', width: '400px'} }>
								{ keys.map(this.renderControl) }
							</div>
						))
					}
				</div>
				<div className="mouse" style={
					{
						border: '1px solid black',
						borderRadius: '35px',
						marginLeft: '20px',
						position: 'relative',
						width: '85px'
					}
				}>
					{ this.renderControl(mouseLeft) }
					{ this.renderControl(mouseRight) }
					{ this.renderControl(mouseCenter) }
				</div>
			</div>
		);
	}
}