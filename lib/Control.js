'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Control = function (_PureComponent) {
	_inherits(Control, _PureComponent);

	function Control() {
		_classCallCheck(this, Control);

		return _possibleConstructorReturn(this, (Control.__proto__ || Object.getPrototypeOf(Control)).apply(this, arguments));
	}

	_createClass(Control, [{
		key: 'render',
		value: function render() {
			return _react2.default.createElement(
				'div',
				{
					onClick: this.props.onClick,
					autoFocus: this.props.autofocus,
					style: _extends({
						backgroundColor: this.props.isSelected ? '#ec9b1d' : 'white',
						color: this.props.isSelected ? 'white' : 'black',
						height: this.props.height + 'px',
						lineHeight: this.props.height + 'px',
						width: this.props.width + 'px',
						border: '1px solid #AAA',
						borderRadius: 3,
						margin: '1px',
						display: 'inline-block',
						cursor: 'pointer',
						fontSize: 10,
						textAlign: 'center'
					}, this.props.inlineStyle)
				},
				this.props.text
			);
		}
	}]);

	return Control;
}(_react.PureComponent);

Control.propTypes = {
	text: _propTypes2.default.oneOfType([_propTypes2.default.string.isRequired, _propTypes2.default.node.isRequired]),
	inlineStyle: _propTypes2.default.object,
	width: _propTypes2.default.number,
	height: _propTypes2.default.number,
	onClick: _propTypes2.default.func.isRequired,
	isSelected: _propTypes2.default.bool
};
Control.defaultProps = {
	inlineStyle: {},
	width: 23,
	height: 16
};
exports.default = Control;
//# sourceMappingURL=Control.js.map