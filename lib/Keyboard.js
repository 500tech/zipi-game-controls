'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Control = require('./Control');

var _Control2 = _interopRequireDefault(_Control);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var row1 = ['Esc', 'F1', 'F2', 'F3', 'F4', 'F5', 'F6', 'F7', 'F8', 'F9', 'F10', 'F11', 'F12'].map(function (k) {
	return { code: k, height: 25 };
}).concat([{
	code: 'power',
	value: 'Power',
	inlineStyle: {
		fontSize: 9,
		flexGrow: 1
	}
}]);

var row2 = ['~', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '-', '+'].map(function (k) {
	return { code: k };
}).concat([{
	code: 'Del',
	inlineStyle: {
		fontSize: 9,
		flexGrow: 1
	}
}]);
var row3 = [{
	code: 'Tab',
	inlineStyle: {
		flexGrow: 1,
		fontSize: 9
	}
}].concat(['Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P', '[', ']', '\\'].map(function (k) {
	return { code: k };
}));

var row4 = [{
	code: 'CAPS',
	inlineStyle: {
		flexGrow: 1,
		fontSize: 9
	}
}].concat(['A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', ';', '\''].map(function (k) {
	return { code: k };
})).concat([{
	code: 'Enter',
	inlineStyle: {
		fontSize: 9,
		flexGrow: 1
	}
}]);

var row5 = [{
	code: 'leftShift',
	value: 'Shift',
	inlineStyle: {
		fontSize: 9,
		flexGrow: 1
	}
}].concat(['Z', 'X', 'C', 'V', 'B', 'N', 'M', ',', '.', '/'].map(function (k) {
	return { code: k };
})).concat([{
	code: 'up',
	value: '/\\',
	inlineStyle: {}
}, {
	code: 'rightShift',
	value: 'Shift',
	inlineStyle: {
		fontSize: 9
	}
}]);

var row6 = [{
	code: 'leftCtrl',
	value: 'Ctrl',
	inlineStyle: {
		fontSize: 9
	}
}, {
	code: 'Fn',
	inlineStyle: {
		fontSize: 9
	}
}, {
	code: 'leftAlt',
	value: 'Alt',
	inlineStyle: {
		fontSize: 9
	}
}, {
	code: 'Space',
	inlineStyle: {
		flexGrow: 1
	}
}, {
	code: 'rightShift',
	value: 'Alt',
	inlineStyle: {
		fontSize: 9
	}
}, {
	code: 'rightCtrl',
	value: 'Ctrl',
	inlineStyle: {
		fontSize: 9
	}
}, {
	code: 'left',
	value: '<',
	inlineStyle: {}
}, {
	code: 'down',
	value: '\\/',
	inlineStyle: {}
}, {
	code: 'right',
	value: '>',
	inlineStyle: {}
}];

var layout = [row1, row2, row3, row4, row5, row6];

var mouseLeft = {
	inlineStyle: {
		position: 'absolute',
		left: '6px',
		top: '7px',
		width: '35px',
		borderRadius: 0,
		borderTopLeftRadius: 26,
		borderColor: 'black',
		height: '50px'
	},
	code: 'mouseLeft',
	value: ' '
};

var mouseCenter = {
	inlineStyle: {
		position: 'absolute',
		left: '34px',
		top: '14px',
		width: '15px',
		borderRadius: '20px',
		height: '30px'
	},
	code: 'mouseCenter',
	value: ' '
};

var mouseRight = {
	inlineStyle: {
		position: 'absolute',
		left: '42px',
		top: '7px',
		width: '34px',
		borderRadius: 0,
		borderTopRightRadius: 26,
		borderColor: 'black',
		height: '50px'
	},
	code: 'mouseRight',
	value: ' '
};

var Keyboard = function (_PureComponent) {
	_inherits(Keyboard, _PureComponent);

	function Keyboard() {
		var _ref;

		var _temp, _this, _ret;

		_classCallCheck(this, Keyboard);

		for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
			args[_key] = arguments[_key];
		}

		return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Keyboard.__proto__ || Object.getPrototypeOf(Keyboard)).call.apply(_ref, [this].concat(args))), _this), _this.isSelected = function (code) {
			return _this.props.selectedControls[code];
		}, _this.handleClick = function (code) {
			return function () {
				if (_this.props.onSelect) {
					_this.props.onSelect(code, !_this.isSelected(code));
				}
			};
		}, _this.renderControl = function (control) {
			return _react2.default.createElement(_Control2.default, {
				key: control.code,
				isSelected: _this.isSelected(control.code),
				text: control.value || control.code,
				onClick: _this.handleClick(control.code),
				inlineStyle: control.inlineStyle,
				width: control.width });
		}, _temp), _possibleConstructorReturn(_this, _ret);
	}

	_createClass(Keyboard, [{
		key: 'render',
		value: function render() {
			var _this2 = this;

			return _react2.default.createElement(
				'div',
				{ className: 'controls', style: { display: 'flex' } },
				_react2.default.createElement(
					'div',
					{ className: 'keyboard' },
					layout.map(function (keys, index) {
						return _react2.default.createElement(
							'div',
							{ key: index, style: { display: 'flex', width: '400px' } },
							keys.map(_this2.renderControl)
						);
					})
				),
				_react2.default.createElement(
					'div',
					{ className: 'mouse', style: {
							border: '1px solid black',
							borderRadius: '35px',
							marginLeft: '20px',
							position: 'relative',
							width: '85px'
						} },
					this.renderControl(mouseLeft),
					this.renderControl(mouseRight),
					this.renderControl(mouseCenter)
				)
			);
		}
	}]);

	return Keyboard;
}(_react.PureComponent);

exports.default = Keyboard;
//# sourceMappingURL=Keyboard.js.map