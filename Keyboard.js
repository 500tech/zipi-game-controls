var Keyboard = require('./lib/Keyboard');
var Control = require('./lib/Control');

module.exports = Keyboard.default || Keyboard;
module.exports.Control = Control.default || Control;
